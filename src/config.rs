use std::path::PathBuf;
use serde_derive::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub log_target: PathBuf,
}