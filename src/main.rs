#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate structopt;

use std::io;
use tui::backend::{TermionBackend, Backend};
use termion::raw::IntoRawMode;

mod event;
mod input;
mod util;
mod config;

mod ui;

use self::ui::*;
use self::input::*;
use self::event::*;

type Result<T> = std::result::Result<T, failure::Error>;

use failure::bail;

use structopt::StructOpt;
use std::path::Path;
use std::io::Read;
use std::env;
use std::path::PathBuf;
use gilrs::{Gilrs, EventType};
use crate::ui::Ui;
use ds::{Alliance, DriverStation};
use std::fs::File;
use chrono::Local;

#[derive(Debug, StructOpt)]
#[structopt(name = "consoleds", about = "A TUI Driver Station for controlling FRC Robots", rename_all = "kebab-case")]
struct Args {
    #[structopt(short, long)]
    team_number: u32,
    #[structopt(long)]
    target: Option<String>,
    #[structopt(short = "c", long)]
    alliance_colour: String,
    #[structopt(short = "p", long)]
    alliance_position: u8,
}

fn log_target_from_config() -> Result<PathBuf> {
    use xdg::BaseDirectories;

    let xdg_dirs = BaseDirectories::with_prefix("consoleds")?;
    match xdg_dirs.find_config_file("config.toml") {
        Some(config_file) => {
            let mut f = File::open(config_file)?;
            let mut s = String::new();
            f.read_to_string(&mut s)?;
            let conf = toml::from_str::<config::Config>(&s)?;
            println!("{:?}", conf);
            Ok(conf.log_target.canonicalize().unwrap())
        }
        None => {
            let mut parent = xdg_dirs.get_config_home();
            parent.push(Path::new("config.toml"));
            File::create(parent);
            Ok(env::current_dir()?.to_path_buf())
        }
    }

}

fn main() -> Result<()> {
    let conf = Args::from_args();
    let alliance = {
        let pos = if conf.alliance_position < 1 || conf.alliance_position > 3 {
            println!("Invalid position \"{}\". Must be between 1 and 3", conf.alliance_position);
            return Ok(());
        } else {
            conf.alliance_position
        };

        match conf.alliance_colour.to_lowercase().as_str() {
            "red" => Alliance::new_red(pos),
            "blue" => Alliance::new_blue(pos),
            _ => {
                println!("Invalid alliance colour. Got \"{}\". Expected \"red\" or \"blue\"", conf.alliance_colour);
                return Ok(());
            }
        }
    };
    let mut ds = if let Some(ref target) = &conf.target {
        DriverStation::new(&target, conf.team_number, alliance)
    } else {
        DriverStation::new_team(conf.team_number, alliance)
    };

    let log_parent = log_target_from_config()?;
    println!("{:?}", log_parent.join(format!("stdout-{}.log", Local::now())));
    let mut log_file = File::create(log_parent.join(format!("stdout-{}.log", Local::now())))?;

    // Go into raw mode and initialize tui-rs backend w/ Termion
    let stdout = io::stdout().into_raw_mode()?;

    let mut ui = Ui::new(stdout, conf.team_number)?;

    ui.run(ds, log_file)
}
