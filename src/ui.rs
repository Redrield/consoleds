use std::io::{Stdout, Write};
use tui::{Terminal, Frame};
use tui::backend::{TermionBackend, Backend};
use termion::raw::RawTerminal;
use crate::Result;
use ds::{DriverStation, TcpPacket, Mode};
use crate::event::{Events, Event};
use tui::layout::{Rect, Layout, Constraint, Direction};
use tui::widgets::{Paragraph, Text, Block, Widget, Borders, List};
use tui::style::*;
use std::sync::{Arc, Mutex};
use crate::input::{gil_ticker, joystick_callback};
use std::thread;
use termion::event::Key;
use std::fs::File;

mod views;

pub struct Ui {
    term: Terminal<TermionBackend<RawTerminal<Stdout>>>,
    team_number: u32
}

struct RobotStdout {
    messages: Vec<String>,
    list_height: usize,
    list_width: usize,
}

impl RobotStdout {
    fn new() -> RobotStdout {
        RobotStdout {
            messages: Vec::new(),
            list_height: 0,
            list_width: 0
        }
    }

    fn add_message(&mut self, mut msg: String) {
        // Some preprocessing because List won't split automatically
        // if the message is too big for the block we're gonna give it, then we're going to manually split it off
        // Ideally, we'll make it happen at the first whitespace boundary. Otherwise, wherever the message overflows.
        if self.list_width != 0 && msg.len() > self.list_width {
            let idx = msg[0..self.list_width - 1].rfind(' ').unwrap_or(self.list_width - 1);
            let msg2 = msg.split_off(idx);
            self.messages.push(msg);
            self.messages.push(msg2);
        } else {
            self.messages.push(msg);
        }
    }

    fn render<B: Backend>(&mut self, frame: &mut Frame<B>, rect: Rect) {
        let area = Block::default().borders(Borders::ALL).title("stdout")
            .inner(rect);

        self.list_height = area.height as usize;
        self.list_width = area.width as usize;

        if self.messages.len() >= self.list_height {
            let overflow = self.messages.len() - self.list_height;
            self.messages.drain(0..overflow);
        }

        let items = self.messages.iter().map(|s| Text::raw(format!("{}\n", s)));
        List::new(items)
            .block(Block::default().borders(Borders::ALL).title("stdout"))
            .render(frame, rect)
//        Paragraph::new(items.iter())
//            .block(Block::default().borders(Borders::ALL).title("stdout"))
//            .wrap(true)
//            .render(frame, rect)
    }
}

impl Ui {
    pub fn new(stdout: RawTerminal<Stdout>, team_number: u32) -> Result<Ui> {
        let mut term = Terminal::new(TermionBackend::new(stdout))?;
        term.clear()?;
        term.hide_cursor()?;

        Ok(Ui { term, team_number  })
    }

    pub fn run(&mut self, mut ds: DriverStation, mut log_file: File) -> Result<()> {
        let mut events = Events::new();
        let team_number = self.team_number;

        thread::spawn(|| {
            gil_ticker();
        });

        let mut stdout = Arc::new(Mutex::new(RobotStdout::new()));

        let mut stdout_tcp = stdout.clone();
        ds.set_tcp_consumer(move |packet| {
            let TcpPacket::Stdout(msg) = packet;
            log_file.write_all(format!("[{:.4}] {}\n", msg.timestamp, msg.message).as_bytes());
            stdout_tcp.lock().unwrap().add_message(msg.message);
        });

        ds.set_joystick_supplier(joystick_callback);

        let mut past_voltages = vec![];

        loop {
            self.term.draw(|mut f| {
                let toplevel = Layout::default()
                    .constraints([
                        Constraint::Percentage(20),
                        Constraint::Percentage(80)
                    ].as_ref())
                    .direction(Direction::Vertical)
                    .split(f.size());


                // Sorta gross, but creates a hierarchy for what should be displayed, ending with Disabled.
                let trace = ds.trace();
                let t = if !trace.is_connected() {
                    Text::styled("No Connection", Style::default().fg(Color::Red).modifier(Modifier::Italic))
                } else if !trace.is_code_started() {
                    Text::styled("No Robot Code", Style::default().fg(Color::Red))
                } else if ds.estopped() {
                    Text::styled("Emergency Stopped", Style::default().fg(Color::Red).modifier(Modifier::Blink))
                } else if ds.enabled() {
                    Text::styled("Enabled", Style::default().fg(Color::Yellow))
                } else {
                    Text::raw("Disabled")
                };

                let headers = Layout::default()
                    .constraints([
                        Constraint::Percentage(30),
                        Constraint::Percentage(20),
                        Constraint::Percentage(20),
                        Constraint::Percentage(30),
                    ].as_ref())
                    .direction(Direction::Horizontal)
                    .split(toplevel[0]);

                views::draw_status(&mut f, t, headers[0]);

                let mode = match ds.mode() {
                    Mode::Autonomous => "Autonomous",
                    Mode::Teleoperated => "Teleoperated",
                    Mode::Test => "Test"
                };

                views::draw_mode(&mut f, mode, headers[1]);
                views::draw_team_info(&mut f, team_number, headers[2]);

                let voltage = ds.battery_voltage();
                past_voltages.push(voltage.round() as u64);

                views::draw_voltages(&mut f, &mut past_voltages, voltage, headers[3]);

                let body = Layout::default()
                    .constraints([
                        Constraint::Percentage(50),
                        Constraint::Percentage(50),
                    ].as_ref())
                    .direction(Direction::Horizontal)
                    .split(toplevel[1]);

                views::draw_instructions(&mut f, body[0]);

                stdout.lock().unwrap().render(&mut f, body[1]);
            })?;

            match events.next()? {
                Event::Input(input) => match input {
                    Key::Char('q') => return Ok(()),
                    Key::Char('e') => ds.enable(),
                    Key::Char('\n') => ds.disable(),
                    Key::Char(' ') => ds.estop(),
                    Key::Ctrl('c') => ds.restart_code(),
                    Key::Ctrl('r') => ds.restart_roborio(),
                    Key::Char('a') => ds.set_mode(Mode::Autonomous),
                    Key::Char('t') => ds.set_mode(Mode::Teleoperated),
                    Key::Char('T') => ds.set_mode(Mode::Test),
                    _ => {}
                }
                Event::Tick => {}
            }
        }
    }
}

impl Drop for Ui {
    fn drop(&mut self) {
        self.term.clear().unwrap();
        self.term.show_cursor().unwrap();
    }
}